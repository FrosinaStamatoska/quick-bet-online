<?php
/**
 * Created by PhpStorm.
 * User: frosinatamatoska
 * Date: 2.10.17
 * Time: 23:14
 */

namespace App\Factories;



use App\Services\DecimalToFractionConverter;
use App\Services\FromDecimalConverter;
use App\Services\FromFractionalConverter;
use App\Services\FromMoneylineConverter;
use App\Services\StoreOddsServiceInterface;

class OddTypeConverterFactory implements TypeConverterFactoryInterface
{
    private $storeOddsService;
    private $decimalToFractionConverter;

    public function __construct(StoreOddsServiceInterface $storeOddsService, DecimalToFractionConverter $decimalToFractionConverter)
    {
        $this->storeOddsService = $storeOddsService;
        $this->decimalToFractionConverter = $decimalToFractionConverter;
    }

    public function returnConverter($type)
    {
        switch ($type) {
            case 'decimal': {
                return new FromDecimalConverter($this->storeOddsService, $this->decimalToFractionConverter);
            }
            case 'fractional': {
                return new FromFractionalConverter($this->storeOddsService);
            }
            case 'moneyline': {
                return new FromMoneylineConverter($this->storeOddsService, $this->decimalToFractionConverter);
            }
        }
    }
}