<?php
/**
 * Created by PhpStorm.
 * User: frosinatamatoska
 * Date: 2.10.17
 * Time: 23:12
 */

namespace App\Factories;


interface TypeConverterFactoryInterface
{
    public function returnConverter($type);
}