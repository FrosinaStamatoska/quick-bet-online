<?php
/**
 * Created by PhpStorm.
 * User: frosinatamatoska
 * Date: 2.10.17
 * Time: 22:40
 */

namespace App\Http\Controllers;


use App\Factories\TypeConverterFactoryInterface;
use App\Http\Requests\OddConversionRequest;
use App\Models\OddConversion;
use App\Services\DatabaseExistenceChecker;
use App\Services\ConverterInterface;
use App\Services\LogWriterInterface;
use Illuminate\Foundation\Validation\ValidatesRequests;

class OddsController extends Controller
{
    private $existenceChecker;
    private $converterFactory;
    private $logWriter;

    public function __construct(DatabaseExistenceChecker $existenceChecker, TypeConverterFactoryInterface $converterFactory, LogWriterInterface $logWriter)
    {
        $this->existenceChecker = $existenceChecker;
        $this->converterFactory = $converterFactory;
        $this->logWriter = $logWriter;
    }

    public function getConvertedValues(OddConversionRequest $request)
    {
        $value = $request->get('value');
        $type = $request->get('type');

        $result = $this->existenceChecker->check($value, $type);
        if ($result) {
            return $this->formatResponse($result->conversion);
        }

        /** @var ConverterInterface $converter */
        $converter = $this->converterFactory->returnConverter($type);

        $result = $converter->convert($value);

        $this->logWriter->store([
            'conversion' => $result,
            'ip_address' => $request->ip()
        ]);

        return $this->formatResponse($result);
    }


    private function formatResponse(OddConversion $conversion)
    {
        return $conversion->load('decimal', 'moneyline', 'fractional');
    }

    public function home()
    {
        return view('home');
    }
}