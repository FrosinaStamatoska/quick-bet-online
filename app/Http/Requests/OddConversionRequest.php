<?php
/**
 * Created by PhpStorm.
 * User: frosinatamatoska
 * Date: 3.10.17
 * Time: 00:39
 */

namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class OddConversionRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function wantsJson()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'type' => 'required',
            'value' => ['required']
        ];

        switch ($this->request->get('type')) {
            case 'decimal': {
                array_push($rules['value'], 'regex:/[1-9]+\.[0-9]+/');
                break;
            }
            case 'fractional': {
                array_push($rules['value'], 'regex:/[0-9]+\/[0-9]+/');
                break;
            }
            case 'moneyline': {
                array_push($rules['value'], 'regex:/^(\+|\-)[0-9]+/');
                break;
            }
        }
        return $rules;
    }

    public function messages()
    {
        return [
            'type.required' => 'The type is required',
            'value.required' => 'The value is required.',
            'value.regex' => 'Invalid format for selected type.'
        ];
    }

}