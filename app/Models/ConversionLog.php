<?php
/**
 * Created by PhpStorm.
 * User: frosinatamatoska
 * Date: 2.10.17
 * Time: 20:49
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class ConversionLog extends Model
{
    protected $table = 'conversion_logs';

    public function oddsConversion()
    {
        return $this->belongsTo(OddConversion::class, 'odds_conversion_id');
    }

}