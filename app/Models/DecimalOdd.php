<?php
/**
 * Created by PhpStorm.
 * User: frosinatamatoska
 * Date: 2.10.17
 * Time: 20:46
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class DecimalOdd extends Model
{
    protected $table = 'decimal_odds';

    public $timestamps = false;

    public function conversion()
    {
        return $this->hasOne(OddConversion::class, 'decimal_odds_id');
    }

}