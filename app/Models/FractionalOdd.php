<?php
/**
 * Created by PhpStorm.
 * User: frosinatamatoska
 * Date: 2.10.17
 * Time: 20:46
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class FractionalOdd extends Model
{
    protected $table = 'fractional_odds';

    public $timestamps = false;

    public function conversion()
    {
        return $this->hasOne(OddConversion::class, 'fractional_odds_id');
    }

}