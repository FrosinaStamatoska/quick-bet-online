<?php
/**
 * Created by PhpStorm.
 * User: frosinatamatoska
 * Date: 2.10.17
 * Time: 20:47
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MoneylineOdd extends Model
{
    protected $table = 'moneyline_odds';

    public $timestamps = false;

    public function conversion()
    {
        return $this->hasOne(OddConversion::class, 'moneyline_odds_id');
    }

}