<?php
/**
 * Created by PhpStorm.
 * User: frosinatamatoska
 * Date: 2.10.17
 * Time: 20:47
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OddConversion extends Model
{
    protected $table = 'odds_conversion';
    public $timestamps = false;

    public function decimal()
    {
        return $this->belongsTo(DecimalOdd::class, 'decimal_odds_id');
    }

    public function fractional()
    {
        return $this->belongsTo(FractionalOdd::class, 'fractional_odds_id');
    }

    public function moneyline()
    {
        return $this->belongsTo(MoneylineOdd::class, 'moneyline_odds_id');
    }

}