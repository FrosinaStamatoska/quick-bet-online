<?php
/**
 * Created by PhpStorm.
 * User: frosinatamatoska
 * Date: 2.10.17
 * Time: 22:55
 */

namespace App\Providers;


use App\Factories\OddTypeConverterFactory;
use App\Factories\TypeConverterFactoryInterface;
use App\Models\ConversionLog;
use App\Models\DecimalOdd;
use App\Models\FractionalOdd;
use App\Models\MoneylineOdd;
use App\Models\OddConversion;
use App\Repositories\EloquentOddConverterRepository;
use App\Repositories\OddConverterRepository;
use App\Services\DatabaseExistenceChecker;
use App\Services\ExistenceChecker;
use App\Services\IpAddressLogWriter;
use App\Services\LogWriterInterface;
use App\Services\StoreOddsService;
use App\Services\StoreOddsServiceInterface;
use Illuminate\Support\ServiceProvider;

class OddsServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(OddConverterRepository::class, EloquentOddConverterRepository::class);
        $this->app->bind(ExistenceChecker::class, DatabaseExistenceChecker::class);
        $this->app->bind(TypeConverterFactoryInterface::class, OddTypeConverterFactory::class);

        $this->app->bind(StoreOddsServiceInterface::class, function () {
            return new StoreOddsService(
                new FractionalOdd(),
                new DecimalOdd(),
                new MoneylineOdd(),
                new OddConversion());
        });
        $this->app->bind(LogWriterInterface::class, function () {
            return new IpAddressLogWriter(new ConversionLog());
        });
    }

}