<?php
/**
 * Created by PhpStorm.
 * User: frosinatamatoska
 * Date: 2.10.17
 * Time: 22:54
 */

namespace App\Repositories;


use App\Models\DecimalOdd;
use App\Models\FractionalOdd;
use App\Models\MoneylineOdd;

class EloquentOddConverterRepository implements OddConverterRepository
{

    public function findDecimal($value)
    {
        return DecimalOdd::with('conversion')->where('value', $value)->first();
    }

    public function findFractional($value)
    {
        return FractionalOdd::with('conversion')->where('value', $value)->first();
    }

    public function findMoneyline($value)
    {
        return MoneylineOdd::with('conversion')->where('value', $value)->first();
    }
}