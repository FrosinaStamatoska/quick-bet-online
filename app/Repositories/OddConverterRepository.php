<?php
/**
 * Created by PhpStorm.
 * User: frosinatamatoska
 * Date: 2.10.17
 * Time: 22:53
 */

namespace App\Repositories;


interface OddConverterRepository
{
    public function findDecimal($value);

    public function findFractional($value);

    public function findMoneyline($value);

}