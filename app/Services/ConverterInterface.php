<?php
/**
 * Created by PhpStorm.
 * User: frosinatamatoska
 * Date: 2.10.17
 * Time: 22:41
 */

namespace App\Services;


interface ConverterInterface
{
    public function convert(string $value);
}