<?php
/**
 * Created by PhpStorm.
 * User: frosinatamatoska
 * Date: 2.10.17
 * Time: 23:03
 */

namespace App\Services;


use App\Repositories\OddConverterRepository;

class DatabaseExistenceChecker implements ExistenceChecker
{

    private $oddConverterRepository;

    public function __construct(OddConverterRepository $oddConverterRepository)
    {
        $this->oddConverterRepository = $oddConverterRepository;
    }

    public function check(string $value, string $type)
    {
        $result = null;

        switch ($type) {
            case 'fractional': {
                $result = $this->oddConverterRepository->findFractional($value);
                break;
            }
            case 'decimal': {
                $result = $this->oddConverterRepository->findDecimal($value);
                break;
            }
            case 'moneyline': {
                $result = $this->oddConverterRepository->findMoneyline($value);
                break;
            }
        }

        return $result;
    }
}