<?php
/**
 * Created by PhpStorm.
 * User: frosinatamatoska
 * Date: 2.10.17
 * Time: 23:02
 */

namespace App\Services;


interface ExistenceChecker
{
    public function check(string $value, string $type);
}