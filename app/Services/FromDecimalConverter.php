<?php
/**
 * Created by PhpStorm.
 * User: frosinatamatoska
 * Date: 2.10.17
 * Time: 22:44
 */

namespace App\Services;


class FromDecimalConverter implements ConverterInterface
{
    private $storeOddsService;
    private $decimalToFractionConverter;

    public function __construct(StoreOddsServiceInterface $storeOddsService, DecimalToFractionConverter $decimalToFractionConverter)
    {
        $this->storeOddsService = $storeOddsService;
        $this->decimalToFractionConverter = $decimalToFractionConverter;
    }

    public function convert(string $value)
    {
        $fractionQuotient = (float)$value - 1;

        $fraction = $this->decimalToFractionConverter->convert($fractionQuotient);


        $parts = explode('/', $fraction);
        $dividend = $parts[0];
        $divisor = $parts[1];

        $prefix = $dividend > $divisor ? '+' : '-';

        $quotient = ($dividend > $divisor) ? (float)($dividend / $divisor) : (float)($divisor / $dividend);

        $moneylineValue = $prefix . ($quotient * 100);

        return $this->storeOddsService->store($fraction, $value, $moneylineValue);
    }
}