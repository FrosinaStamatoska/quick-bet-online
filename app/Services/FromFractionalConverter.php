<?php
/**
 * Created by PhpStorm.
 * User: frosinatamatoska
 * Date: 2.10.17
 * Time: 22:44
 */

namespace App\Services;


class FromFractionalConverter implements ConverterInterface
{

    private $storeOddsService;

    public function __construct(StoreOddsServiceInterface $storeOddsService)
    {
        $this->storeOddsService = $storeOddsService;
    }

    public function convert(string $value)
    {
        $parts = explode('/', $value);
        $dividend = (int)$parts[0];
        $divisor = (int)$parts[1];

        $quotient = (float)$dividend / $divisor;

        $decimalValue = $quotient + 1;

        //prefix for moneyline
        $prefix = ($dividend > $divisor) ? '+' : '-';
        //quotient for moneyline
        $quotient = ($dividend > $divisor) ? (float)($dividend / $divisor) : (float)($divisor / $dividend);

        $moneyLineValue = $prefix . ($quotient * 100);

        return $this->storeOddsService->store($value, $decimalValue, $moneyLineValue);
    }
}