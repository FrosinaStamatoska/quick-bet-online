<?php
/**
 * Created by PhpStorm.
 * User: frosinatamatoska
 * Date: 2.10.17
 * Time: 22:45
 */

namespace App\Services;


class FromMoneylineConverter implements ConverterInterface
{
    private $storeOddsService;
    private $decimalToFractionConverter;

    public function __construct(StoreOddsServiceInterface $storeOddsService, DecimalToFractionConverter $decimalToFractionConverter)
    {
        $this->storeOddsService = $storeOddsService;
        $this->decimalToFractionConverter = $decimalToFractionConverter;
    }

    public function convert(string $value)
    {
        $sign = $value[0];
        $number = substr($value, 1);

        if ($sign === '+') {
            $quotient = (float)$number / 100;
        } else {
            $quotient = (float)100 / $number;
        }

        $fraction = $this->decimalToFractionConverter->convert($quotient);

        $decimalValue = $quotient + 1;

        return $this->storeOddsService->store($fraction, $decimalValue, $value);
    }

}