<?php
/**
 * Created by PhpStorm.
 * User: frosinatamatoska
 * Date: 3.10.17
 * Time: 00:48
 */

namespace App\Services;


use App\Models\ConversionLog;

class IpAddressLogWriter implements LogWriterInterface
{
    private $conversionLog;

    public function __construct(ConversionLog $conversionLog)
    {
        $this->conversionLog = $conversionLog;
    }

    public function store($data)
    {
        $conversion = $data['conversion'];
        $ipAddress = $data['ip_address'];

        $this->conversionLog->ip_address = $ipAddress;
        $this->conversionLog->oddsConversion()->associate($conversion);
        $this->conversionLog->save();
    }
}