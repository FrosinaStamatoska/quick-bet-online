<?php
/**
 * Created by PhpStorm.
 * User: frosinatamatoska
 * Date: 3.10.17
 * Time: 00:47
 */

namespace App\Services;


interface LogWriterInterface
{
    public function store($data);
}