<?php
/**
 * Created by PhpStorm.
 * User: frosinatamatoska
 * Date: 3.10.17
 * Time: 00:24
 */

namespace App\Services;


use App\Models\ConversionLog;
use App\Models\DecimalOdd;
use App\Models\FractionalOdd;
use App\Models\MoneylineOdd;
use App\Models\OddConversion;

class StoreOddsService implements StoreOddsServiceInterface
{
    private $fractional;
    private $decimal;
    private $moneyline;
    private $conversion;

    public function __construct(FractionalOdd $fractional, DecimalOdd $decimal, MoneylineOdd $moneyline, OddConversion $conversion)
    {
        $this->conversion = $conversion;
        $this->fractional = $fractional;
        $this->decimal = $decimal;
        $this->moneyline = $moneyline;
    }

    public function store(string $fractionalValue, string $decimalValue, string $moneylineValue)
    {
        $this->fractional->value = $fractionalValue;
        $this->fractional->save();

        $this->decimal->value = $decimalValue;
        $this->decimal->save();

        $this->moneyline->value = $moneylineValue;
        $this->moneyline->save();

        $this->conversion->moneyline()->associate($this->moneyline);
        $this->conversion->decimal()->associate($this->decimal);
        $this->conversion->fractional()->associate($this->fractional);
        $this->conversion->save();

        return $this->conversion;
    }
}