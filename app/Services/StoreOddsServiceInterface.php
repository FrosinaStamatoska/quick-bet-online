<?php
/**
 * Created by PhpStorm.
 * User: frosinatamatoska
 * Date: 3.10.17
 * Time: 00:23
 */

namespace App\Services;


interface StoreOddsServiceInterface
{
    public function store(string $fractional, string $decimal, string $moneyline);
}