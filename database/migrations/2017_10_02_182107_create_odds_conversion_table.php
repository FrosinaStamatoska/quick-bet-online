<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOddsConversionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('odds_conversion', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('fractional_odds_id')->nullable();

            $table->foreign('fractional_odds_id')
                ->references('id')->on('fractional_odds')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->unsignedInteger('decimal_odds_id')->nullable();

            $table->foreign('decimal_odds_id')
                ->references('id')->on('decimal_odds')
                ->onUpdate('cascade')->onDelete('cascade');


            $table->unsignedInteger('moneyline_odds_id')->nullable();

            $table->foreign('moneyline_odds_id')
                ->references('id')->on('moneyline_odds')
                ->onUpdate('cascade')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('odds_conversion');
    }
}
