<?php

use App\Models\DecimalOdd;
use App\Models\FractionalOdd;
use App\Models\MoneylineOdd;
use App\Models\OddConversion;
use Illuminate\Database\Seeder;
use Illuminate\Filesystem\Filesystem;

class InsertOddsSeeder extends Seeder
{
    private $filesystem;
    private $storeOddsService;

    public function __construct(Filesystem $filesystem, \App\Services\StoreOddsServiceInterface $storeOddsService)
    {
        $this->filesystem = $filesystem;
        $this->storeOddsService = $storeOddsService;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $contents = $this->filesystem->get(storage_path('app/odds.csv'));

        $lines = explode(PHP_EOL, $contents);

        foreach ($lines as $line) {
            $items = explode(',', $line);

            $this->storeOddsService->store($items[0], $items[1], $items[2]);

        }


    }
}
