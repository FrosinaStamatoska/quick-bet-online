$(document).ready(function () {
    var type = 'fractional';
    var value;

    $('.errors').hide();
    $('.results').hide();
    $('#myModal').modal({backdrop: 'static', keyboard: false});

    $('.dropdown-menu li a').on('click', function () {
        var text = $(this).html();
        type = $(this).attr('data-type');
        $('.type-label').html(text);
        $('.errors').hide();
        $('.results').hide();
    });

    $('.value').on('change', function () {
        $('.errors').hide();
        $('.results').hide();
    });

    $('.close-img').on('click', function () {
        $('#myModal').modal('hide');
    });


    $('.btn-go').on('click', function () {
        value = $('.value').val();

        if (!validate()) {
            $('.error-message').html('Invalid value format.');
            $('.errors').show();
            return;
        }

        makeConversionApiCall();
    });

    function makeConversionApiCall() {
        var data = {
            type: type,
            value: value
        };
        $.ajax({
            url: '/api/convert',
            data: data,
            dataType: 'json',
            method: 'POST',
            headers: {
                'X-Requested-With': 'XMLHttpRequest'
            },
            success: function (response) {
                handleConversionResults(response);
            }
        });
    }

    function handleConversionResults(response) {
        $('.results').show();

        $('.decimal-value').html(response.decimal.value);
        $('.fractional-value').html(response.fractional.value);
        $('.moneyline-value').html(response.moneyline.value);

        $('.decimal').parent().show();
        $('.fractional').parent().show();
        $('.moneyline').parent().show();

        $('.' + type).parent().hide();
    }

    function validate() {
        switch (type) {
            case 'decimal': {
                return value.match('[1-9]+\\.[0-9]+');
            }
            case 'fractional': {
                return value.match('[0-9]+\\/[0-9]+');
            }
            case 'moneyline': {
                return value.match('^(\\+|\\-)[0-9]+');
            }
        }
    }
});