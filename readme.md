## Run Application
    After you clone the project, enter in the directory of the project and run the following commands.
    
        composer install
    
    Create your .env file, change your database credentials and run the following commands. 
    
        php artisan migrate
        php artisan db:seed
    
    To start local server: 
        php artisan serve

## API Calls

    For converting odds you should specify the type and the value that needs to be converted
       
       1. url: /api/convert
       2. data: 
        -   type => [decimal, fractional, moneyline]
        -   value => [depending of the format of the type] 
       
## Odds conversion calculator
    
    Access the homepage and you will be shown modal,  where you need to choose an type and enter a value that you want to convert. After that you need to click Convert.