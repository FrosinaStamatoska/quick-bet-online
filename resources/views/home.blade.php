<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description"
          content="Use our odds conversion calculator.
          Odds conversion from Fractional, Decimal or Moneyline formats. Quick and always accurate result.">
    <meta name="keywords"
          content="odds,bettings,moneyline,fractional,decimal,quick-bet,uk,eu,usa,calculator,conversion,converter">

    <title>BETTING ODDS CONVERSION! - QuickBetOnline</title>

    <meta property="og:title" content="Bettings odds conversion - QuickBetOnline"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="http://127.0.0.1:8000"/>
    <meta property="og:image" content="{{URL::to('/images/dice.png')}}"/>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link href="{{URL::to('/bower_components/bootstrap/dist/css/bootstrap.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{URL::to('/bower_components/bootstrap/dist/css/bootstrap-theme.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{URL::to('/css/quick-bet.css')}}" rel="stylesheet" type="text/css"/>
</head>
<body>
<div class="container container-fluid">
    @include('modals/home-modal')
</div>

<script type="application/javascript" src="{{URL::to('/bower_components/jquery/dist/jquery.js')}}"></script>
<script type="application/javascript" src="{{URL::to('/bower_components/bootstrap/dist/js/bootstrap.js')}}"></script>
<script type="application/javascript" src="{{URL::to('/js/quick-bet.js')}}"></script>
</body>
</html>
