<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <img alt="Dice" src="{{URL::to('/images/dice.png')}}" class="dice-img"/>
                <img alt="Close sign" src="{{URL::to('/images/close.png')}}" class="close-img"/>
            </div>
            <div class="modal-body">
                <div class="row black-container">
                    <h5 class="modal-title text-center">BETTING ODDS CONVERSION!</h5>
                </div>
                <div class="row">
                    <div class="col-md-6 col-xs-12 col-sm-12">
                        <div class="form-group">
                            <label>From:</label>
                            <div class="dropdown type-dropdown">
                                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <span class="type-label">Fractional odds (UK)</span>
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                    <li><a href="#" data-type="fractional">Fractional odds (UK)</a></li>
                                    <li><a href="#" data-type="decimal">Decimal odds (EU)</a></li>
                                    <li><a href="#" data-type="moneyline">Moneyline odds (USA)</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-12 col-sm-12">
                        <div class="form-group">
                            <label for="value">Value:</label>
                            <input class="form-control value" id="value">
                        </div>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12">

                        <div class="form-group pull-right">
                            <button class="form-control btn-go btn btn-default">Convert!</button>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 errors">
                        <span class="error-message"></span>
                    </div>
                </div>
                <div class="row results">
                    <div class="col-xs-12 col-sm-12 col-md-4">
                        <div class="fractional row">
                            <label class="col-xs-12 col-md-12 col-sm-12">Fractional</label>
                            <div class="col-xs-12 col-md-12 col-sm-12">
                                <span class="text-muted fractional-value"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4">
                        <div class="decimal row">
                            <label class="col-xs-12 col-md-12 col-sm-12">Decimal</label>
                            <div class="col-xs-12 col-md-12 col-sm-12">
                                <span class="text-muted decimal-value"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4">
                        <div class="moneyline row">
                            <label class="col-xs-12 col-md-12 col-sm-12">Moneyline</label>
                            <div class="col-xs-12 col-md-12 col-sm-12">
                                <span class="text-muted moneyline-value"></span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>
